"use strict";
var helpers = require('../../helpers/helpers');
var isDefined = helpers.isDefined;

var structureRestaurant = {
  "password" : "",
  "email" : "",
  "name" : "",
  "phone":"",
  "address":"",
  "social":{
    "facebook" : "",
    "twitter" : "",
    "instagram" : ""
  },
  "localization":{
    "coordinates": []
  },
  "website" : ""
};

function validateRestaurants(structure, next){
  var data = [];
  var testAuthorize = true;
  if(isDefined(structure.email)){
    data.push({
      "email":{
        "status" : "ok",
        "value" : structure.email
      }
    });
  }else{
    data.push({
      "email":{
        "status" : "error",
        "value" : "the email is undefined"
      }
    });
    testAuthorize = false;
  }
  if(isDefined(structure.password)){
    data.push({
      "password":{
        "status" : "ok",
        "value" : structure.password
      }
    });
  }else{
    data.push({
      "password":{
        "status" : "error",
        "value" : "the password is undefined"
      }
    });

    testAuthorize = false;
  }
  if(isDefined(structure.name)){
    data.push({
      "name":{
        "status" : "ok",
        "value" : structure.name
      }
    });
  }else{
    data.push({
      "name":{
        "status" : "error",
        "value" : "the name is undefined"
      }
    });

    testAuthorize = false;
  }
  next(testAuthorize, data);
}

function resToIncorrectStructure(req, res, data){
  console.log("llega aqui");
  if(isDefined(req.query.errors) && req.query.errors=="verbose"){
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureRestaurant to be sent",
          "error" : {
            data:data
          },
          "help" : {
            "message" : " you structureRestaurant object must have the following structure",
            "structure": structureRestaurant
          }
        }
      });
  }else{
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureRestaurant to be sent",
          "help" : {
            "message" : " you structureRestaurant object must have the following structure",
            "structure": structureRestaurant
          }
        }
      });
  }
}

module.exports = {
  validateRestaurants : validateRestaurants,
  resToIncorrectStructure : resToIncorrectStructure
};
