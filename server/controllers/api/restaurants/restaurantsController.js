"use strict"
var mongoose = require('mongoose');
var restaurantModel = require('../../../models/restaurant').restaurant;
var plateModel = require('../../../models/menu').plate;
var helpers = require('../../helpers/helpers');
var auth = helpers.authorization;
var validateStructure = require('./restaurantsValidation');
var personalCodesStatus = require('./personalCodeClient');
var async = require('async');
var service = require('../../helpers/service');

//add new restaurant
function add(req, res){
  var validate = validateStructure.validateRestaurants;
  validate(req.body, function(textAuthorized, data){
    if(textAuthorized){
      var newRestaurant = new restaurantModel({
        email : req.body.email,
        password : helpers.encrypt(req.body.password),
        name:req.body.name
      });
      if(req.body.phone){
        newRestaurant.phone = req.body.phone
      }
      if(req.body.address){
        newRestaurant.address = req.body.address
      }
      if(req.body.website){
        newRestaurant.website = req.body.website
      }
      if(req.body.social){
        if(req.body.social.twitter){
          newRestaurant.social.twitter = req.body.social.twitter
        }
        if(req.body.social.instagram){
          newRestaurant.social.instagram = req.body.social.instagram
        }
        if(req.body.social.facebook){
          newRestaurant.social.facebook = req.body.social.facebook
        }
      }
      restaurantModel.create(newRestaurant, function(err, restaurant){
        console.log(err);
        if(!err){
            var user = {
                _id:restaurant._id,
                email:restaurant.email,
                name: restaurant.name,
                rol:"restaurant"
            };
            var token = service.createToken(user);
            restaurant.password = undefined;
            res.status(201).send({ token:token });
        }else{
            if(err.code == 11000){
              personalCodesStatus.res409(res);
            }else{
              personalCodesStatus.res500(res);
            }
          }
      });
    }else{
      validateStructure.resToIncorrectStructure(req, res, data);
    }
  });
}

//get all Restaurants in the data base
function search(req, res){
  var query ={};
  if(req.query.first){
    query .name .first=req.query.first;
  }
  if(req.query.last){
    query .name .last=req.query.last;
  }
  restaurantModel.find(query, {password:0}).sort("name").exec(function(err, restaurants){
    if(!err){
      res
      .status(200).send({restaurants : restaurants});
    }else{
      personalCodesStatus.res500(res);
    }
  });
}

//get one Restaurant in the data base by email
function find(req, res){
    if(helpers.isObjectId(req.params.restaurant)){
        restaurantModel.findOne({_id:req.params.restaurant}, {password:0}, function(err, restaurant){
            if(!err){
                res.status(200).send({restaurant : restaurant});
            }else{
                personalCodesStatus.res500(res);
            }
        });
    }else{
        personalCodesStatus.res400(res);
    }
}

//update one criterion
function updateOneCriterion(req, res, model){
    if(req.body.name){
        model.name = req.body.name;
    }
    if(req.body.social){
        if(req.body.social.facebook){
            model.social.facebook = req.body.social.facebook;
        }
        if(req.body.social.twitter){
            model.social.twitter = req.body.social.twitter;
        }
        if(req.body.social.instagram){
            model.social.instagram = req.body.social.instagram;
        }
    }
    if(req.body.website){
        model.website = req.body.website;
    }
    if(req.body.phone){
        model.phone = req.body.phone;
    }
    if(req.body.address) {
        model.address = req.body.address;
    }
    if(req.body.pass){
        var passSha1 = helpers.encrypt(req.body.pass);
        model.password = passSha1;
    }
    if(req.body.localization){
        if(req.body.localization.coordinates){
            var localization = req.body.localization.coordinates;
            if(Array.isArray(localization)){
                if(!isNaN(localization[0]) && !isNaN(localization[1])){
                    model.localization.coordinates = localization;
                }
            }else{
                if(typeof localization === "string"){
                    if(localization.indexOf(',') != -1){
                        localization = localization.split(',');
                        if(!isNaN(localization[0]) && !isNaN(localization[1])){
                            model.localization.coordinates = localization;
                        }
                    }
                }
            }
        }
    }
    model.save(function(err, restaurant){
    if(!err){
      res
        .status(200)
        .send({restaurant : restaurant});
    }else{
      personalCodesStatus.res500(res);
    }
    });
}

function update(req, res){
    var restaurantId = req.params.restaurant;
    if(helpers.isObjectId(restaurantId)){
        var token = req.headers.authorization;
        auth(token, restaurantId, "restaurant", function(err){
            if(!err){
                restaurantModel.findOne({_id: restaurantId}, function(err, restaurant){
                    if(!err){
                      if(restaurant){
                        updateOneCriterion(req, res, restaurant);
                      }else{
                        personalCodesStatus.res404(res);
                      }
                    }else{
                      personalCodesStatus.res500(res);
                    }
                });
            }else{
                personalCodesStatus.res401(res);
            }
        });
    }else{
        personalCodesStatus.res400(res);
    }
}

//delete restaurant
function remove(req, res){
    var restaurantId = req.params.restaurant;
    if(helpers.isObjectId(restaurantId)){
        var token = req.headers.authorization;
        auth(token, restaurantId, "restaurant", function(err){
            if(!err){
                async.waterfall([
                    function findRestaurant(next){
                        restaurantModel.findOne({_id:restaurantId}, function(err, restaurant){
                            if(!err){
                                if(restaurant){
                                    next(null, restaurant);
                                }else{
                                    next(404);
                                }
                            }else{
                                next(500);
                            }
                        });
                    },
                    function removeRestaurant(restaurant, next){
                        restaurant.remove(function(err){
                            if(!err){
                                next();
                            }else{
                                next(500);
                            }
                        });
                    },
                    function findPlates(next){
                        plateModel.find({restaurant:restaurantId}, function(err, plates){
                            if(err) return personalCodesStatus.res500(res);
                            next(null, plates);
                        });
                    },
                    function removePlates(plates, next){
                        if(plates && plates.length != 0){
                            plate.remove();
                            next();
                        }else{
                            next();
                        }
                    }
                ],function(err){
                    switch (err) {
                        case 500:
                            personalCodesStatus.res500(res);
                            break;
                        case 404:
                            personalCodesStatus.res404(res);
                            break;
                        default:
                            res.status(200).send({"message":"the restaurant was removed"});
                            break;
                    }
                });
            }else{
                personalCodesStatus.res401(res);
            }
        });
    }else{
        personalCodesStatus.res400(res);
    }
}


module.exports = {
  add : add,
  search : search,
  find : find,
  update : update,
  remove : remove
}
