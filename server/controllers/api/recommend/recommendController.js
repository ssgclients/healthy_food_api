'use strict';
var helpers = require("../../helpers/helpers");
var service = require("../../helpers/service");
var restaurantModel = require("../../../models/restaurant").restaurant;
var clientModel = require("../../../models/clients").client;
var plateModel = require("../../../models/menu").plate;
var async = require('async');
var http = require('http');
var _ = require('lodash');

function InternalServerError(res, message){
    return res.status(500).send({
        "error":{
            "error":"Internal Server Error",
            "message":message
        }
    });
}

function BadRequest(res, message){
    return res.status(400).send({
        "error":{
            "error":"Bad Request",
            "message":message
        }
    });
}

function NotFound(res, message){
    return res.status(404).send({
        "error":{
            "error":"Not Found",
            "message":message
        }
    });
}

function Unauthorized(res, message){
  return res.status(401).send({
        "error":{
          "error": "Unauthorized",
          "info" : message
        }
    });
}

function distanceByApiGoogle(origin, destinations, next){
    http.get({
        host: 'maps.googleapis.com',
        path: '/maps/api/distancematrix/json?origins='+origin+'&destinations='+destinations
    }, function(response) {
        var body = '';
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {
            var parsed = JSON.parse(body);
            next(parsed);
        });
    });
}


function doWaterfall1(req, res, pRestaurants){
    var token = service.decodeToken(req.headers.authorization);
    async.waterfall([
        function searchOriginAndDestinations(next){
            if(pRestaurants && pRestaurants.length != 0){
                var origin = req.query.latitude +','+ req.query.longitude;
                var destinations = "";
                async.each(pRestaurants, function(restaurant, callback){
                    destinations += restaurant.localization.coordinates[0]+','+restaurant.localization.coordinates[1]+'|';
                    callback(null);
                },function(err){
                    next(null, pRestaurants, origin, destinations);
                });
            }else{
                next(NotFound(res, "Restaurants not exists"));
            }
        },
        function searchByApiGoogle(restaurants, origin, destinations, next){
            if(origin !="" && destinations != ""){
                distanceByApiGoogle(origin, destinations, function(distance){
                    if(!distance) return next(InternalServerError(res, "bad request api de google"));
                    next(null,restaurants, distance.rows[0].elements);
                });
            }else{
                next(BadRequest(res, "Origin and destinations not exist"));
            }
        },
        function mergeRestaurantAndDistance(restaurants, distance, next){
            var mergeRestaurants = [];
            async.forEachOf(restaurants, function(restaurant, index, callback){
                if(helpers.isDefined(distance[index].distance)){
                    var object = {};
                    object.restaurant = restaurant._id;
                    object.distance = distance[index].distance;
                    mergeRestaurants.push(object);
                    callback(null);
                }else{
                    callback(null);
                }
            }, function(err){
                var array = [];
                async.each(mergeRestaurants, function(res, callback){
                    array.push(res.restaurant);
                    callback(null);
                }, function(err){
                    next(null, array, mergeRestaurants);
                });
            });
        },
        function searhPlates(restaurants,restaurantDistancia, next){
            plateModel.find({restaurant:{$in:restaurants}}).populate("restaurant", "-password").exec(function(err, plates){
                if(err) return next(InternalServerError(res, "Server error"));
                next(null,restaurantDistancia, plates);
            });
        },
        function addAttrDistancePlate(restaurants, plates, next){
            if(plates.length != 0){
                var plateDistance = [];
                async.each(plates, function(plate, callback){
                    async.each(restaurants, function(restaurant, callback){
                        if(plate.restaurant._id.equals(restaurant.restaurant)){
                            var mergePlate = {
                                plate:plate,
                                distance:restaurant.distance
                            };
                            plateDistance.push(mergePlate);
                            callback(null);
                        }else{
                            callback(null);
                        }
                    }, function(err){
                        callback(null);
                    });
                }, function(err){
                    next(null, plateDistance);
                });
            }else{
                next(null, plates);
            }
        },
        function searchDiseases(plates, next){
            clientModel.findOne({_id:token.sub}).populate("diseases").exec(function(err, client){
                if(err) return next(Unauthorized(res,"This error occurs when you don't have authorization"));
                if(!client) return next(NotFound(res, "Client don't exist"));
                //llamar a una nueva funcion
                next(null,plates,client.diseases);
            });
        },
        function removePlates(plates, diseases, next){
            if(plates.length != 0 && diseases.length !=0){
                var _plates = [];
                async.each(plates, function(item, callback){
                    async.each(diseases, function(disease, callback2){
                        var isBad =_.intersection(item.plate.ingredients, disease.noRecommended).length != 0;
                        if(!isBad && req.query.withoutIngredients){
                            isBad = _.intersection(item.plate.ingredients, req.query.withoutIngredients.split(",")).length != 0;
                        }

                        if(isBad){
                            callback2("error");
                        }else{
                            callback2(null);
                        }
                    }, function(err){
                        if(err){
                            callback(null);
                        }else{
                            _plates.push(item);
                            callback(null);
                        }
                    });
                }, function(err){
                    next(null, _plates);
                });
            }else{
                next(null, plates);
            }
        },
        function orderByDistance(plates, next){
            async.sortBy(plates, function(plate, callback){
                callback(null, plate.distance.value);
            }, function(err, result){
                next(null, result);
            });
        }
    ],
    function(err, plates){
        if(err) return err;
        res.send(plates);
    });
}

exports.search = function(req, res){
    var token = service.decodeToken(req.headers.authorization);
    if(token.rol == "client"){
        if(req.query.latitude && req.query.longitude){
            if(helpers.isDefinedOrEmpty(req.query.restaurants)){
                var restaurants = helpers.isDefinedOrEmpty(req.query.restaurants) ? helpers.removeWhiteSpace(req.query.restaurants.split(',')) : "";
                var query = restaurants != "" ? {_id:{$in:restaurants}} : "";
                restaurantModel.find(query)
                    .limit(20)
                    .exec(function(err, restaurants){
                        if(err) return InternalServerError(res, "Server error");
                        doWaterfall1(req, res, restaurants);
                    });
            }else {
                restaurantModel.find({
                    localization: {
                        $near : {
                            $geometry: {
                                type: "Point",
                                coordinates: [
                                    Number(req.query.longitude),
                                    Number(req.query.latitude)
                                 ]
                            }
                        }
                    },
                    "localization.coordinates":{ $ne : [0,0] }
                })
                .limit(20)
                .exec(function(err, restaurants){
                    if(err) return InternalServerError(res, "Server error");
                    doWaterfall1(req, res, restaurants);
                });
            }
        }else{
            BadRequest(res, "The request contains bad syntax");
        }
    }else{
        Unauthorized(res,"This error occurs when you don't have authorization");
    }
}
