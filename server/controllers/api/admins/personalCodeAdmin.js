function res409(res){
  return res .status(409)
              .send({
                "error":{
                  "error": "resourceAlreadyExists",
                  "info" : "This error occurs when an admin is logged exists"
                }
              });
}

function res404(res){
  return res .status(404)
          .send({
            "error":{
              "error": "resourceDoesntExists",
              "info" : "This error occurs when an admin doesn't exists"
            }
          });
}


function res500(res){
  return res.status(500)
            .send({
              "error":{
                "error": "somethingIsWrongWithUs",
                "info" : "This error occurs with our server, we expect fix soon"
              }
            });
}

function res401(res){
  return res.status(401)
          .send({
            "error":{
              "error": "Unauthorized",
              "info" : "This error occurs when you don't have authorization"
            }
          });
}

module.exports = {
  res409 : res409,
  res404 : res404,
  res500 : res500,
  res401 : res401
};