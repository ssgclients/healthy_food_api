var mongoose = require('mongoose');
var adminsModel = require('../../../models/admin').admin;
var validateStructure = require('./admins_validation');
var helpers = require('../../helpers/helpers');
var service = require('../../helpers/service');
var personalCodesStatus = require('./personalCodeAdmin');

//add new admin
function add(req, res){
  helpers.authorization(req.headers.authorization, null,"admin", function(err, token){
    if(!err){
      var validate = validateStructure.validateAdmins;
      validateStructure.validateAdmins(req.body, function(testAuthorized, data){
        if(testAuthorized){
          var passSha1 = helpers.encrypt((req.body.password).toLowerCase());
          var newAdmin = new adminsModel({
            email:(req.body.email).toLowerCase(),
            password: passSha1
          });
          if(req.body.name){
            newAdmin.first = (req.body.name.first).toLowerCase() || '',
            newAdmin.last = (req.body.name.last).toLowerCase() || ''
          }
          adminsModel.create(newAdmin, function(err, admin){
              if(!err){
                admin.password = undefined;
                res.status(201).send({adminCreated: admin});
              }else{
                if(err.code == 11000){
                  personalCodesStatus.res409(res);
                }else{
                  personalCodesStatus.res500(res);
                }
              }
            }
          );
        }else{
          validateStructure.resToIncorrectStructure(req, res, data);
        }
      });
    }else{
      personalCodesStatus.res401(res);
    }
  });
}

function authorizationAdmin(auth, next){
  var token = service.decodeToken(auth);
  adminsModel.findOne({_id:token.sub}, function(err, admin){
    next(err, admin);
  });
}

//get all admins in the data base
function search(req, res){
  authorizationAdmin(req.headers.authorization, function(err, admin){
    if(err) return personalCodesStatus.res500(res);
    if(admin){
      adminsModel.find({}, {password:0}, function(err, admins){
        if(!err){
          res.status(200).send({ admins:admins});
        }else{
          personalCodesStatus.res500(res);
        }
      });
    }else{
      personalCodesStatus.res401(res);
    }
  });
}


//get one admin in the data base
function find(req, res){
  authorizationAdmin(req.headers.authorization, function(err, admin){
    if(err) return personalCodesStatus.res500(res);
    if(admin){
      adminsModel.findOne({_id:req.params.admin}, {password:0}, function(err, admin){
        if(!err){
          res.status(200).send({admin:admin});
        }else{
          personalCodesStatus.res500(res);
        }
      });
    }else{
      personalCodesStatus.res401(res);
    }
  });
}

//update one criterion
function updateOneCriterion(req, res, model){
  var isDefined = helpers.isDefined;
  var info = {};
  if (isDefined(req.body.name)){
    if (isDefined(req.body.name.first)) {
     model.name.first= req.body.name.first;
    }
    if (isDefined(req.body.name.last)) {
      model.name.last= req.body.name.last;
    }
  }
  if (isDefined(req.body.password)) {
    var passSha1 = helpers.encrypt(req.body.password);
    model.password = passSha1;
  }
  model.save(function(err, admin){
    if(!err){
      res
        .status(200)
        .send({admin : admin});
    }else{
      personalCodesStatus.res500(res);
    }
  });
}

//update admin
function update(req, res){
  helpers.authorization(req.headers.authorization, req.params.admin,"admin", function(err, token){
    if(!err){
      adminsModel.findOne({_id: req.params.admin}, function(err, admin){
        if(!err){
          console.log(admin);
          if(admin){
            updateOneCriterion(req, res, admin);
          }else{
            personalCodesStatus.res404(res);
          }
        }else{
          personalCodesStatus.res500(res);
        }
      });
    }else{
      personalCodesStatus.res401(res);
    }
  });
}

//delete admin
function remove(req, res){
  helpers.authorization(req.headers.authorization, req.params.admin,"admin", function(err, token){
    if(!err){
      adminsModel.findOne({_id:token.sub}, function(err, admin){
        if(!err){
          if(admin){
            admin.remove(function(err){
              if(!err){
                res
                .status(200)
                .send({
                  "message":"the admin was removed"
                });
              }else{
                personalCodesStatus.res500(res);
              }
            });
          }else{
            personalCodesStatus.res404(res);
          }
        }else{
          personalCodesStatus.res500(res);
        }
      });
    }else{
      personalCodesStatus.res401(res);
    }
  });
}

module.exports = {
  add : add,
  search : search,
  find : find,
  update : update,
  remove : remove
};
