"use strict";
var helpers = require('../../helpers/helpers');
var isDefined = helpers.isDefined;

var structureAdmin = {
  "email" : "",
  "password" : "",
  "name" : {
    "first" : "",
    "last" : ""
  }
};

function validateAdmins(structure, next){
  console.log('validando', structure);
  var data = [];
  var testAuthorize = true;
  if(isDefined(structure.email)){
    data.push({
      "email":{
        "status" : "ok",
        "value" : structure.email
      }
    });
  }else{
    data.push({
      "email":{
        "status" : "error",
        "value" : "the email is undefined"
      }
    });
    testAuthorize = false;
  }
  if(isDefined(structure.password)){
    data.push({
      "password":{
        "status" : "ok",
        "value" : structure.password
      }
    });
  }else{
    data.push({
      "password":{
        "status" : "error",
        "value" : "the password is undefined"
      }
    });

    testAuthorize = false;
  }
  next(testAuthorize, data);
}


function resToIncorrectStructure(req, res, data){
  if(isDefined(req.query.errors) && req.query.errors=="verbose"){
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureAdmin to be sent",
          "error" : {
            data:data
          },
          "help" : {
            "message" : " you structureAdmin object must have the following structure",
            "structure": structureAdmin
          }
        }
      });
  }else{
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureAdmin to be sent",
          "help" : {
            "message" : " you structureAdmin object must have the following structure",
            "structure": structureAdmin
          }
        }
      });
  }
}


module.exports = {
  validateAdmins : validateAdmins,
  resToIncorrectStructure : resToIncorrectStructure
};