"use strict";
var helpers = require('../../helpers/helpers');
var isDefined = helpers.isDefined;

var structureClient = {
  "email" : "",
  "password" : "",
  "name" : {
    "first" : "",
    "last" : ""
  },
  "phone":"",
  "address":""
};

function validateClients(structure, next){
  console.log('validando', structure);
  var data = [];
  var testAuthorize = true;
  if(isDefined(structure.email)){
    data.push({
      "email":{
        "status" : "ok",
        "value" : structure.email
      }
    });
  }else{
    data.push({
      "email":{
        "status" : "error",
        "value" : "the email is undefined"
      }
    });
    testAuthorize = false;
  }
  if(isDefined(structure.password)){
    data.push({
      "password":{
        "status" : "ok",
        "value" : structure.password
      }
    });
  }else{
    data.push({
      "password":{
        "status" : "error",
        "value" : "the user is undefined"
      }
    });

    testAuthorize = false;
  }
  next(testAuthorize, data);
}

function resToIncorrectStructure(req, res, data){
  if(isDefined(req.query.errors) && req.query.errors=="verbose"){
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureclient to be sent",
          "error" : {
            data:data
          },
          "help" : {
            "message" : " you structureClient object must have the following structure",
            "structure": structureClient
          }
        }
      });
  }else{
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureClient to be sent",
          "help" : {
            "message" : " you structureClient object must have the following structure",
            "structure": structureClient
          }
        }
      });
  }
}

module.exports = {
  validateClients : validateClients,
  resToIncorrectStructure : resToIncorrectStructure
};