"use strict";
var mongoose = require('mongoose');
var clientsModel = require('../../../models/clients').client;
var diseaseModel = require('../../../models/diseases').disease;
var helpers = require('../../helpers/helpers');
var service = require('../../helpers/service');
var validateStructure = require('./clientsValidation');
var personalCodesStatus = require('./personalCodeClient');
var async = require('async');

function checkDiseases(array, next){
    async.each(array, function(item, callback){
        if(helpers.isObjectId(item)){
            diseaseModel.findOne({_id:item}, function(err, disease){
                if(!err){
                    if(!disease) callback(406);
                    callback();
                }else{
                    callback(err);
                }
            });
        }else{
            callback(400);
        }
    }, function(err){
        if(err) next(err);
        next(null);
    });
}

//add new client
function add(req, res){
  var validate = validateStructure.validateClients;
  validate(req.body, function(testAuthorized, data){
    if(testAuthorized){
      var passSha1 = helpers.encrypt(req.body.password);
      var newClient = {
        email:req.body.email,
        password: passSha1,
        phone : req.body.phone,
        address : req.body.address
      };
      if(req.body.name){
          newClient.name.first  = req.body.name.first || '';
          newClient.name.last  = req.body.name.last || '';
      }
       async.waterfall([
            function(next){
                if(req.body.diseases){
                    var diseases = null;
                    if(helpers.isObjectArray(req.body.diseases)){
                        diseases = req.body.diseases;
                    }else{
                        if(typeof req.body.diseases == "string"){
                            diseases = req.body.diseases.split(',');
                        }
                    }
                    checkDiseases(diseases, function(err){
                        if(!err){
                            model.diseases = diseases;
                            next();
                        }else{
                            next(err);
                        }
                    });
                  }else{
                    next();
                  }
            },
            function saveClient(next){
                clientsModel.create(newClient,function(err, client){
                  if(!err){
                    client.password = undefined;
                    var user = {
                      _id:client._id,
                      email:client.email,
                      rol:"client"
                    };
                    var token = service.createToken(user);
                    res
                      .status(201)
                      .send({clientCreate:client, token:token});
                  }else{
                    if(err.code == 11000){
                      personalCodesStatus.res409(res);
                    }else{
                      personalCodesStatus.res500(res);
                    }
                  }
              });
            }
        ],function(err, client){
            switch (err) {
                case 500:
                    personalCodesStatus.res500(res);
                    break;
                case 400:
                    personalCodesStatus.res400(res);
                    break;
                case 406 :
                    res.status(406).send({"message":"disease does not exist"});
                    break;
                default:
                    res.status(200).send({client: client});
                    break;
            };
        });
    }else{
      validateStructure.resToIncorrectStructure(req, res, data);
    }
  });
}

//get client by criteria
function search(req, res){
  console.log(req.query);

  if(req.query.first && req.query.last){
    var query={
      "name.first":{ $regex: req.query.first},
      "name.last":{ $regex: req.query.last}
    };
  }else{
    if(req.query.first){
      var query={"name.first":{ $regex: req.query.first} };
    }
    if(req.query.last){
      var query={"name.last":{ $regex: req.query.last}};
    }
    if(req.query.email){
      var query={"email":{ $regex: req.query.email}};
    }
  }

  clientsModel.find(query, {password:0}).populate('diseases').sort("name").exec(function(err, clients){
    if(!err){
      res
      .status(200)
      .send({
        clients : clients
      });
    }else{
      personalCodesStatus.res500(res);
    }
  });
}

//get one client in the data base
function find(req, res){
    if(helpers.isObjectId(req.params.client)){
        clientsModel.findOne({_id:req.params.client}, {password:0}).populate('diseases').exec(function(err, client){
            if(!err){
                res.status(200).send({client : client});
            }else{
                personalCodesStatus.res500(res);
            }
        });
    }else{
        personalCodesStatus.res400(res);
    }
}

//update one criterion
function updateOneCriterion(req, res, model){
  var isDefined = helpers.isDefined;
  if (isDefined(req.body.name)){
    if (isDefined(req.body.name.first)) {
     model.name.first= req.body.name.first;
    }
    if (isDefined(req.body.name.last)) {
      model.name.last= req.body.name.last;
    }
  }
  if (isDefined(req.body.phone)) {
    model.phone = req.body.phone;
  }

  if (isDefined(req.body.address)) {
    model.address = req.body.address;
  }
  if (isDefined(req.body.password)) {
    var passSha1 = helpers.encrypt(req.body.password);
    model.password = passSha1;
  }
  async.waterfall([
        function(next){
            if(req.body.diseases){
                var diseases = null;
                if(helpers.isObjectArray(req.body.diseases)){
                    diseases = req.body.diseases;
                }else{
                    if(typeof req.body.diseases == "string"){
                        diseases = req.body.diseases.split(',');
                    }
                }
                checkDiseases(diseases, function(err){
                    if(!err){
                        model.diseases = diseases;
                        next();
                    }else{
                        next(err);
                    }
                });
              }else{
                next();
              }
        },
        function saveClient(next){
            model.save(function(err, clientUpdate){
                if(!err){
                    clientsModel.findOne({_id:clientUpdate._id}).populate('diseases').exec(function(err, client){
                        if(err) next(err);
                        next(null, client);
                    });
                }else{
                    personalCodesStatus.res500(res);
                }
            });
        }
    ],function(err, client){
        switch (err) {
            case 500:
                personalCodesStatus.res500(res);
                break;
            case 400:
                personalCodesStatus.res400(res);
                break;
            case 406 :
                res.status(406).send({"message":"disease does not exist"});
                break;
            default:
                res.status(200).send({client: client});
                break;
        };
    });
}

function update(req, res){
    if(helpers.isObjectId(req.params.client)){
        helpers.authorization(req.headers.authorization, req.params.client, "client", function(err, token){
            if(!err){
                clientsModel.findOne({_id: req.params.client}, function(err, client){
                    if(!err){
                        if(client){
                            updateOneCriterion(req, res, client);
                        }else{
                            personalCodesStatus.res404(res);
                        }
                    }else{
                        personalCodesStatus.res500(res);
                    }
                });
            }else{
                personalCodesStatus.res401(res);
            }
        });
    }else{
        personalCodesStatus.res400(res);
    }
}

//delete client
function remove(req, res){
    if(helpers.isObjectId(req.params.client)){
        helpers.authorization(req.headers.authorization, req.params.client, "client", function(err, token){
            if(!err){
                clientsModel.findOne({_id:req.params.client}, function(err, client){
                    if(!err){
                        if(client){
                            client.remove(function(err){
                                if(!err){
                                    res.status(200).send({"message":"the client was removed"});
                                }else{
                                    personalCodesStatus.res500(res);
                                }
                            });
                        }else{
                            personalCodesStatus.res404(res);
                        }
                    }else{
                        personalCodesStatus.res500(res);
                    }
                });
            }else{
                personalCodesStatus.res401(res);
            }
        });
    }else{
        personalCodesStatus.res400(res);
    }
}

module.exports = {
 search : search,
 add : add,
 find : find,
 update : update,
 remove : remove
}
