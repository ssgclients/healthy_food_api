"use strict"
var mongoose = require('mongoose');
var ingredientModel = require('../../../models/ingredient').ingredient;
var diseaseIngredientModel = require('../../../models/ingredient').deseaseIngredient;
var diseaseModel = require('../../../models/diseases').disease;
var helpers = require('../../helpers/helpers');
var validateStructure = require('./ingredientValidation');
var personalCodesStatus = require('./personalCodeClient');

//add new ingredient
function add(req, res){
  var validate = validateStructure.validateIngredient;
  validate(req.body, function(textAuthorized, data){
    if(textAuthorized){
      ingredientModel.create({
        name:(req.body.name).toLowerCase()
      }, function(err, ingredient){
        if(!err){
          res
            .status(201)
            .send({ingredientCreate:ingredient});
        }else{
          if(err.code == 11000){
            personalCodesStatus.res409(res);
          }else{
            personalCodesStatus.res500(res);
          }
        }
      });
    }else{
      validateStructure.resToIncorrectStructure(req, res, data);
    }
  });
}

//get all ingredients in the data base
function search(req, res){
  ingredientModel.find(function(err, models){
    if(!err){
      res
        .status(200)
        .send({ingredients:models});
    }else{
      personalCodesStatus.res500(res);
    }
  });
}

//get one ingredient in the data base
function find(req, res){
  ingredientModel.findOne({name:(req.params.id).toLowerCase()}, function(err, model){
    if(!err){
      res
        .status(200)
        .send({ingredient:model});
    }else{
      personalCodesStatus.res500(res);
    }
  });
}

//update one criterion
function update(req, res){
  ingredientModel.findOne({_id: req.params.id}, function(err, ingredient){
    if(!err){
      console.log(ingredient);
      if(ingredient){
        updateOneCriterion(req, res, ingredient);
      }else{
        personalCodesStatus.res404(res);
      }
    }else{
      personalCodesStatus.res500(res);
    }
  });
}

function updateOneCriterion(req, res, model){
  var isDefined = helpers.isDefined;

  if (isDefined(req.body.name)) {
    model.name = req.body.name;
  }
  if (isDefined(req.body.description)) {
    model.description = req.body.description;
  }
  model.save(function(err, ingredient){
    if(!err){
      res
        .status(200)
        .send({ingredient : ingredient});
    }else{
      personalCodesStatus.res500(res);
    }
  });
}

//delete ingredient
function remove(req, res){
  ingredientModel.findOne({_id:req.params.id}, function(err, ingredient){
    console.log(err);
    if(!err){
      if(ingredient){
        ingredient.remove(function(err){
          if(!err){
            res
            .status(200)
            .send({
              "message":"the ingredient was removed"
            });
          }else{
            personalCodesStatus.res500(res);
          }
        });
      }else{
        personalCodesStatus.res404(res);
      }
    }else{
      personalCodesStatus.res500(res);
    }
  });
}

module.exports = {
  search : search,
  add : add,
  find : find,
  update : update,
  remove : remove
};