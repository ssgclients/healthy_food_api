"use strict";
var helpers = require('../../helpers/helpers');
var isDefined = helpers.isDefined;

var structurePlate = {
  "name" : "",
  "price" : "",
  "image" : {
    "url" : "",
    "pubId" : ""
  },
  "ingredients" : []
};

function validatePlate(structure, next){
  console.log('validando', structure);
  var data = [];
  var testAuthorize = true;

  if(isDefined(structure.name)){
      data.push({
        "name" : {
          "status" : "ok",
          "value" : structure.name
        }
      });
  }else{
    data.push({
      "name":{
        "status" : "error",
        "value" : "the name is undefined"
      }
    });

    testAuthorize = false;
  }

  if(Array.isArray(structure.ingredients)){
    if(structure.ingredients.length != 0){
        data.push({
          "ingredients" : {
            "status" : "ok",
            "value" : structure.ingredients
          }
        });
    }else{
      data.push({
        "ingredients":{
          "status" : "error",
          "value" : "the ingredients is empty"
        }
      });
      testAuthorize = false;
    }
  }else{
    if(typeof structure.ingredients === "string"){
      var ingredients = structure.ingredients.split(',');
      if(structure.ingredients.length != 0){
          data.push({
            "ingredients" : {
              "status" : "ok",
              "value" : structure.ingredients
            }
          });
      }else{
        data.push({
          "ingredients":{
            "status" : "error",
            "value" : "the ingredients is empty"
          }
        });
        testAuthorize = false;
      }
    }else{
      data.push({
          "ingredients":{
            "status" : "error",
            "value" : "the ingredients is not object array"
          }
      });
      testAuthorize = false;
    }
  }

  next(testAuthorize, data);
}

function resToIncorrectStructure(req, res, data){
  if(isDefined(req.query.errors) && req.query.errors=="verbose"){
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structurePlate to be sent",
          "error" : {
            data:data
          },
          "help" : {
            "message" : " you structurePlate object must have the following structure",
            "structure": structurePlate
          }
        }
      });
  }else{
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structurePlate to be sent",
          "help" : {
            "message" : " you structurePlate object must have the following structure",
            "structure": structurePlate
          }
        }
      });
  }
}

module.exports={
	validatePlate : validatePlate,
	resToIncorrectStructure : resToIncorrectStructure
}
