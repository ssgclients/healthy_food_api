'use strict';
var mongoose = require('mongoose');
var plateModel = require('../../../models/menu').plate;
var restaurantModel = require('../../../models/restaurant').restaurant;
var ingredientModel = require('../../../models/ingredient').ingredient;
var helpers = require('../../helpers/helpers');
var service = require('../../helpers/service');
var validateStructure = require('./plateValidation');
var personalCodesStatus = require('./personalCodePlate');
var cloudinary = require('../../../config/cloudinary');
var async = require('async');

function saveAllIngredients(array, next){
    var length = array.length;
    for (var i = 0; i < length; i++) {
        if(array[i] !== ""){
            ingredientModel.create({name:array[i].toLowerCase()}, function(err){
                if(err){
                    if(err.code != 11000){
                        next(err);
                    }
                }
            });
        }
    };
    next(null);
}

//add plate and menu
function add(req, res){
    if(helpers.isObjectId(req.params.restaurant)){
        helpers.authorization(req.headers.authorization, req.params.restaurant,"restaurant", function(err, token){
            if(!err){
                validateStructure.validatePlate(req.body, function(testAuthorize, data){
                    if(testAuthorize){
                        var ingredients = null;
                        if(Array.isArray(req.body.ingredients)){
                            ingredients = helpers.removeWhiteSpace(req.body.ingredients);
                        }else{
                            ingredients = helpers.removeWhiteSpace(req.body.ingredients.split(','));
                        }
                        async.waterfall([
                            function searchRestaurant(callback){
                                restaurantModel.findOne({'_id':req.params.restaurant}, function(err, restaurant){
                                    if (err) return err;
                                    callback(null, restaurant);
                                });
                            },
                            function uploadImageInCloudinary(restaurant, callback){
                                var newPlate = new plateModel();
                                newPlate.name = req.body.name;
                                newPlate.ingredients = ingredients;
                                newPlate.price = Number(req.body.price);
                                newPlate.restaurant = restaurant._id;
                                if (req.file) {
                                    console.log()
                                    cloudinary.uploader.upload(req.file.path, function(image){
                                        newPlate.image.url = image.url;
                                        newPlate.image.pubId = image.public_id;
                                        callback(null, newPlate);
                                    });
                                } else {
                                    callback(null, newPlate);
                                }
                            },
                            function savePlateinBD(newPlate, callback){
                                newPlate.save(function(err,platedSaved){
                                    if (err) personalCodesStatus.res500(res);
                                    callback(null, platedSaved);
                                });
                            },
                            function saveIngredients(plate, callback){
                                saveAllIngredients(ingredients, function(err, ingredientsSaved){
                                    if (err) callback(err);
                                    callback(null, plate);
                                });
                            },
                            function findPlate(plate, callback){
                                plateModel.findOne({_id:plate._id}).populate('restaurant', '-password').exec(function(err, plateCreate){
                                    if(err) callback(err);
                                    callback(null, plateCreate);
                                });
                            }
                        ], function(err, result){
                            if (err) return personalCodesStatus.res500(res);
                            res.status(201).send(result);
                        });
                    }else{
                        validateStructure.resToIncorrectStructure(req, res, data);
                    }
                });
            }else{
                personalCodesStatus.res401(res);
            }
        });
    }else{
        personalCodesStatus.res400(res);
    }
}


//get all plates or menu from restaurant --
function searchByRestaurant(req, res){
    if(helpers.isObjectId(req.params.restaurant)){
        if(req.query.pagination == "false"){
            plateModel
            .find({restaurant:req.params.restaurant})
            .populate("restaurant", '-password')
            .sort({
                name: 'asc'
            })
            .exec(function(err, menu){
                if(!err){
                        res.status(200).send({menu:menu});
                }else{
                    personalCodesStatus.res500(res);
                }
            });
        }else{
            var numRows = 10;
            if(req.query.numRows){
                if(req.query.numRows > 0 && !isNaN(req.query.numRows)){
                   numRows = req.query.numRows;
                }
            }
            var page = numRows;
            if(req.query.page){
                if(req.query.page > 0 && !isNaN(req.query.page)){
                   page = req.query.page*numRows;
                }
            }
            plateModel
            .find({restaurant:req.params.restaurant})
            .populate("restaurant", '-password')
            .limit(numRows)
            .skip(page-numRows)
            .sort({
                name: 'asc'
            })
            .exec(function(err, menu){
                if(!err){
                        res.status(200).send({menu:menu});
                }else{
                    personalCodesStatus.res500(res);
                }
            });
        }
    }else{
        personalCodesStatus.res400(res);
    }
}

function search(req, res){
    var query = {};
    if(req.query.name){
        query.name = {$regex: req.query.name};
    }
    var restaurant = "restaurant";
    if(req.query.restaurant=="false"){
        restaurant = "";
    }

    if(req.query.pagination == "false"){
        plateModel.find(query)
        .populate(restaurant, '-password')
        .sort({
            name: 'asc'
        })
        .exec(function(err, plates){
            if(!err){
                res.status(200).send({plates:plates});
            }else{
                personalCodesStatus.res500(res);
            }
        });
    }else{
        var numRows = 10;
        if(req.query.numRows){
            if(req.query.numRows > 0 && !isNaN(req.query.numRows)){
               numRows = req.query.numRows;
            }
        }
        var page = numRows;
        if(req.query.page){
            if(req.query.page > 0 && !isNaN(req.query.page)){
               page = req.query.page*numRows;
            }
        }
        plateModel.find(query)
        .populate(restaurant, '-password')
        .limit(numRows)
        .skip(page-numRows)
        .sort({
            name: 'asc'
        })
        .exec(function(err, plates){
            if(!err){
                res.status(200).send({plates:plates});
            }else{
                personalCodesStatus.res500(res);
            }
        });
    }
}

//get one plate from restaurant
function find(req, res){
    if(helpers.isObjectId(req.params.plate)){
        var restaurant = "restaurant";
        if(req.query.restaurant=="false"){
            restaurant = "";
        }
        plateModel.findOne({_id:req.params.plate}).populate(restaurant, '-password').exec(function(err, plate){
            if(!err){
                    res.status(200).send({plate:plate});
            }else{
                personalCodesStatus.res500(res);
            }
        });
    }else{
        personalCodesStatus.res400(res);
    }
}

function remove(req, res){
    if(helpers.isObjectId(req.params.plate)){
        var token = service.decodeToken(req.headers.authorization);
        async.waterfall([
            function searchDish(callback){
                plateModel.findOne({restaurant: token.sub, _id: req.params.plate},function(err, plate){
                    if (err) callback(personalCodesStatus.res500(res));
                    if (!plate) callback(personalCodesStatus.res404(res));
                    console.log(plate);
                    callback(null, plate);
                });
            },
            function removeDish(plate, callback){
                    plate.remove();
                    if(plate.image.pubId!=="v1454019945")cloudinary.uploader.destroy(plate.image.pubId);
                    callback(null,{'message':'the plate was removed'})
            }
        ], function(err,result){
            if (err) return err;
            res.status(200).send(result);
        });
    }else{
        personalCodesStatus.res400(res);
    }
}

function update(req, res){
	if(helpers.isObjectId(req.params.plate)){
		var token = service.decodeToken(req.headers.authorization);
        async.waterfall([
            function findPlate(callback){
                plateModel.findOne({_id: req.params.plate, restaurant: token.sub},function(err, plate){
                    console.log(plate);
                    if(err) callback(personalCodesStatus.res500(res));
                    if (!plate) callback(personalCodesStatus.res404(res));
                    callback(null, plate);
                });
            },
            function updatePlate(plate, callback){
                console.log(plate);
                plate.name = req.body.name || plate.name;
                plate.price = req.body.price || plate.price;
                if(req.body.ingredients){
                    if (Array.isArray(req.body.ingredients)) {
                        plate.ingredients = req.body.ingredients || plate.ingredients;
                    } else {
                        plate.ingredients = req.body.ingredients.split(',') || plate.ingredients;
                    }
                }
                callback(null,plate);
            },
            function updateImageInClodudinary(plate, callback){
                if (req.file) {
                    cloudinary.uploader.destroy(plate.image.pubId);
                    cloudinary.uploader.upload(req.file.path, function(result){
                        plate.image.url = result.url;
                        plate.image.pubId = result.public_id;
                        callback(null, plate);
                    });
                } else {
                    callback(null, plate);
                }
            },
            function savePlate(plate, callback){
                plate.save(function(err, platedSaved){
                    if (err) callback(personalCodesStatus.res500(res));
                    callback(null, platedSaved);
                })
            },
            function findPlate(plate, callback){
                plateModel.findOne({_id:plate._id}).populate('restaurant', '-password').exec(function(err, plateCreate){
                    if(err) callback(err);
                    callback(null, plateCreate);
                });
            }
        ], function(err, result){
            if (err) return err;
            res.status(200).send(result);
        })

    }else {
        personalCodesStatus.res400(res);
    }
}

module.exports = {
	add : add,
	searchByRestaurant : searchByRestaurant,
	find : find,
	remove : remove,
	search : search,
	update : update
};
