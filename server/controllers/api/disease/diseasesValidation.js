"use strict";
var helpers = require('../../helpers/helpers');
var isDefined = helpers.isDefined;

var structureDisease = {
  "name": "",
  "description": "",
  "recommended" : [],
  "noRecommended" : []
};

var structureDiseaseClient = {
  "diseaseId": ""
};

function validateDiseases(structure, next){
  console.log('validando', structure);
  var data = [];
  var testAuthorize = true;
  if(isDefined(structure.name)){
    data.push({
      "name":{
        "status" : "ok",
        "value" : structure.name
      }
    });
  }else{
    data.push({
      "name":{
        "status" : "error",
        "value" : "the name is undefined"
      }
    });
    testAuthorize = false;
  }

  if(helpers.isObjectArray(structure.recommended)){
    if(structure.recommended.length != 0){
        data.push({
          "recommended" : {
            "status" : "ok",
            "value" : structure.recommended
          }
        });
    }else{
      data.push({
        "recommended":{
          "status" : "error",
          "value" : "the recommended is empty"
        }
      });
      testAuthorize = false;
    }
  }else{
    if(typeof structure.recommended === "string"){
      if(structure.recommended.split(',').length != 0){
        data.push({
          "recommended" : {
            "status" : "ok",
            "value" : structure.recommended
          }
        });
      }else{
        data.push({
          "recommended":{
            "status" : "error",
            "value" : "the recommended is empty"
          }
        });
        testAuthorize = false;
      }
    }else{
      data.push({
        "recommended":{
          "status" : "error",
          "value" : "the recommended is not object array"
        }
      });
      testAuthorize = false;
    }
  }

  if(helpers.isObjectArray(structure.noRecommended)){
    if(structure.noRecommended.length != 0){
        data.push({
          "noRecommended" : {
            "status" : "ok",
            "value" : structure.noRecommended
          }
        });
    }else{
      data.push({
        "noRecommended":{
          "status" : "error",
          "value" : "the noRecommended is empty"
        }
      });
      testAuthorize = false;
    }
  }else{
    if(typeof structure.noRecommended === "string"){
      if(structure.noRecommended.split(',').length  != 0){
        data.push({
          "noRecommended" : {
            "status" : "ok",
            "value" : structure.noRecommended
          }
        });
      }else{
        data.push({
          "noRecommended":{
            "status" : "error",
            "value" : "the noRecommended is empty"
          }
        });
        testAuthorize = false;
      }
    }else{
      data.push({
        "noRecommended":{
          "status" : "error",
          "value" : "the noRecommended is not object array"
        }
      });
      testAuthorize = false;
    }
  }

  next(testAuthorize, data);
};

function validateDiseaseClient(structure, next){
  console.log('validando disease client', structure);
  var data = [];
  var testAuthorize = true;
  if(isDefined(structure.diseaseId)){
    if(helpers.isObjectId(structure.diseaseId)){
      data.push({
        "diseaseId":{
          "status" : "ok",
          "value" : structure.diseaseId
        }
      });
    }else{
      data.push({
        "diseaseId":{
          "status": "error",
          "value" : "the id disease is not object id"
        }
      });
      testAuthorize = false;
    }
  }else{
    data.push({
      "diseaseId":{
        "status": "error",
        "value" : "the id disease is undefined"
      }
    });
    testAuthorize = false;
  }
  next(testAuthorize, data);
}


function resToIncorrectStructure(req, res, data){
  if(isDefined(req.query.errors) && req.query.errors=="verbose"){
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureDisease to be sent",
          "error" : {
            data:data
          },
          "help" : {
            "message" : " you structureDisease object must have the following structure",
            "structure": structureDisease
          }
        }
      });
  }else{
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureDisease to be sent",
          "help" : {
            "message" : " you structureDisease object must have the following structure",
            "structure": structureDisease
          }
        }
      });
  }
}

function resToIncorrectStructureDiseaseClient(req, res, data){
  if(isDefined(req.query.errors) && req.query.errors=="verbose"){
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureDiseaseClient to be sent",
          "error" : {
            data:data
          },
          "help" : {
            "message" : " you structureDiseaseClient object must have the following structure",
            "structure": structureDiseaseClient
          }
        }
      });
  }else{
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureDiseaseClient to be sent",
          "help" : {
            "message" : " you structureDiseaseClient object must have the following structure",
            "structure": structureDiseaseClient
          }
        }
      });
  }
}

module.exports = {
  validateDiseases : validateDiseases,
  resToIncorrectStructure : resToIncorrectStructure ,
  validateDiseaseClient : validateDiseaseClient,
  resToIncorrectStructureDiseaseClient : resToIncorrectStructureDiseaseClient
};