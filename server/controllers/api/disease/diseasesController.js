'use strict';
var mongoose = require('mongoose');
var diseaseModel  =  require('../../../models/diseases').disease;
var clientModel = require('../../../models/clients').client;
var ingredientModel = require('../../../models/ingredient').ingredient;
var helpers = require('../../helpers/helpers');
var validateStructure = require('./diseasesValidation');
var service = require('../../helpers/service');
var authorization = helpers.authorization;
var personalCodesStatus = require('./personalCodeDiseases');

function saveAllIngredients(array, next){
	var length = array.length;
	for (var i = 0; i < length; i++) {
		if(array[i] !== ""){
			ingredientModel.create({name:array[i]}, function(err){
				if(err){
					if(err.code !== 11000){
						console.log(err);
						next(err);
					}
				}
			});
		}
	};
	next(null);
}

//add diseases
function add(req, res){
	var validation = validateStructure.validateDiseases;
	validation(req.body, function(testAuthorize, data){
		if(testAuthorize){
			var recommended = req.body.recommended;
			if(typeof recommended == "string"){
				recommended = helpers.removeWhiteSpace(recommended.split(','));
			}else{
                recommended = helpers.removeWhiteSpace(recommended);
            }
			saveAllIngredients(recommended, function(err){
				if(!err){
					var noRecommended = req.body.noRecommended;
					if(typeof noRecommended == "string"){
						noRecommended = helpers.removeWhiteSpace(noRecommended.split(','));
					}else{
                        noRecommended = helpers.removeWhiteSpace(noRecommended);
                    }
					saveAllIngredients(noRecommended, function(err){
						if(!err){
							diseaseModel.create({
								name:req.body.name.toLowerCase(),
								description : req.body.description,
								recommended:recommended,
								noRecommended : noRecommended
							}, function(err, disease){
								if(!err){
									res.status(201).send({diseaseCreate:disease});
								}else{
									if(err.code != 11000){
										personalCodesStatus.res500(res);
									}else{
										personalCodesStatus.res409(res);
									}
								}
							});
						}else{
							personalCodesStatus.res500(res);
						}
					});
				}else{
					personalCodesStatus.res500(res);
				}
			});
		}else{
			validateStructure.resToIncorrectStructure(req, res, data);
		}
	});
}

//get diseases
function search(req, res){
	diseaseModel.find(function(err, diseases){
		if(!err){
			if(diseases){
				res
					.status(200)
					.send({diseases:diseases});
			}else{
				personalCodesStatus.res404(res);
			}
		}else{
			personalCodesStatus.res500(res);
		}
	});
}

//get one disease
function find(req, res){
	if(helpers.isObjectId(req.params.disease)){
		diseaseModel.findOne({_id : req.params.disease}, function(err, disease){
			if(!err){
				if(disease){
					res.status(200).send({disease:disease});
				}else{
					personalCodesStatus.res404(res);
				}
			}else{
				personalCodesStatus.res500(res);
			}
		});
	}else{
		personalCodesStatus.res400(res);
	}
}

//update one criterion
function updateOneCriterion(req, res, model){
  var isDefined = helpers.isDefined;
  if (isDefined(req.body.name)) {
    model.name = req.body.name;
  }
  if (isDefined(req.body.description)) {
    model.description = req.body.description;
  }
  if(isDefined(req.body.recommended)){
  	var recommended = req.body.recommended;
  	if(typeof recommended == "string"){
  		recommended = helpers.removeWhiteSpace(recommended.split(','));
  		if(recommended.length != 0){
  			model.recommended = recommended;
  		}
  	}else{
  		if(recommended.length != 0){
  			model.recommended = recommended;
  		}
  	}
  }

  if(isDefined(req.body.noRecommended)){
  	var noRecommended = req.body.noRecommended;
  	if(typeof noRecommended == "string"){
  		noRecommended = helpers.removeWhiteSpace(noRecommended.split(','));
  		if(noRecommended.length != 0){
  			model.noRecommended = noRecommended;
  		}
  	}else{
  		if(noRecommended.length != 0){
  			model.noRecommended = noRecommended;
  		}
  	}
  }

  model.save(function(err, disease){
    if(!err){
      res.status(200).send({disease : disease});
    }else{
      personalCodesStatus.res500(res);
    }
  });
}

function update(req, res){
	if(helpers.isObjectId(req.params.disease)){
	  	diseaseModel.findOne({_id: req.params.disease}, function(err, disease){
		    if(!err){
		      if(disease){
		        updateOneCriterion(req, res, disease);
		      }else{
		        personalCodesStatus.res404(res);
		      }
		    }else{
		      personalCodesStatus.res500(res);
		    }
	  	});
	}else{
		personalCodesStatus.res400(res);
	}
}

//delete disease
function remove(req, res){
	if(helpers.isObjectId(req.params.disease)){
		diseaseModel.findOne({_id:req.params.disease}, function(err, disease){
		    if(!err){
		      if(disease){
		        disease.remove(function(err){
		          if(!err){
		            res
		            .status(200)
		            .send({
		              "message":"the disease was removed"
		            });
		          }else{
		            personalCodesStatus.res500(res);
		          }
		        });
		      }else{
		        personalCodesStatus.res404(res);
		      }
		    }else{
		      personalCodesStatus.res500(res);
		    }
		});
	}else{
		personalCodesStatus.res400(res);
	}
}

module.exports = {
	add : add,
	search : search,
	find : find,
	update : update,
	remove : remove
};