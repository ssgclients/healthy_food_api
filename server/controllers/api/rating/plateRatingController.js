'use strict';
var mongoose = require("mongoose");
var personalCodeStatus = require("./personalCodeRating");
var ratingModel = require("../../../models/menu").plateRating;
var plateModel = require("../../../models/menu").plate;
var restaurantModel = require("../../../models/restaurant").restaurant;
var clientModel = require("../../../models/clients").client;
var helpers = require('../../helpers/helpers');
var service = require('../../helpers/service');
var async = require('async');

function add(req, res){
	var token = service.decodeToken(req.headers.authorization);
	if(token.rol === "client"){
		var value  = req.body.value || 0;
		if(isNaN(value)) return personalCodeStatus.res400(res);
		if(value < 0 || value > 5) return personalCodeStatus.res400(res);
		value = Number(value);
		async.waterfall([
			function findClient(next){
				clientModel.findOne({_id:token.sub}, function(err, client){
					if(!err){
						if(!client) next(personalCodeStatus.res404(res, "Client don't exist"));
						next();
					}else{
						next(personalCodeStatus.res500(res));
					}
				});
			},
			function findClient(next){
				plateModel.findOne({_id:req.params.plate}, function(err, plate){
					if(!err){
						if(!plate) next(personalCodeStatus.res404(res, "Plate don't exist"));
						next(null,plate);
					}else{
						next(personalCodeStatus.res500(res));
					}
				});
			},
			function findRating(plate, next){
				ratingModel.findOne({plate:plate._id, client:token.sub}, function(err, rating){
					if(!err){
						if(rating){
							next(null, true, rating, plate);
						}else{
							next(null, false, rating, plate);
						}
					}else{
						next(personalCodeStatus.res500(res));
					}
				});
			},
			function updateRating(exist, rating, plate, next){
				if(exist){
					plate.rating.sum = plate.rating.sum - rating.value + value;
					rating.value = value;
					rating.save();
					plate.save();
					next(null,true,plate);
				}else{
					next(null, false, plate);
				}
			},
			function saveRating(exist, plate, next){
				if(!exist){
					var newRating = new ratingModel({
						plate:plate._id,
						client: token.sub,
						value:value
					});
					newRating.save();
					plate.rating.sum = plate.rating.sum + value;
					plate.rating.countClients = plate.rating.countClients+1;
					plate.save();
					next(null,plate);
				}else{
					next(null,plate);
				}
			}
		],
		function(err, plate){
			if(err) return err;
			res.status(200).send(plate);
		});
	}else{
		personalCodeStatus.res401(res);
	}
}

function search(req, res){
	var query = {};
	if(req.query.client){
		if(helpers.isObjectId(req.query.client)){
			query.client = req.query.client;
		}
	}
	if(req.query.plate){
		if(helpers.isObjectId(req.query.plate)){
			query.plate = req.query.plate;
		}
	}
	var restaurant = "";
	if(req.query.restaurant == "true"){
		restaurant = 'plate.restaurant';
	}
	console.log(query);

	ratingModel.find(query).populate("client plate", "-password").exec(function(err, ratings){
		if(err) return personalCodeStatus.res500(res);
		plateModel.populate(
			ratings,{
			path: restaurant,
		    model: restaurantModel},
		    function(err, ratings){
				res.status(200).send(ratings);
			}
		);
	});
}

module.exports = {
	add : add,
	search : search
};