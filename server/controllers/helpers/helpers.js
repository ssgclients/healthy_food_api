"use strict";
var crypto = require('crypto');
var mongoose = require('mongoose');
var service = require('./service');

exports.isDefined = function (params){
  if( typeof params ==="undefined" || params === null){
    return false;
  }else{
    return true;
  }
}


exports.isDefinedOrEmpty = function (params){
  if( typeof params ==="undefined" || params === null || params.trim() === ""){
    return false;
  }else{
    return true;
  }
}

exports.encrypt = function (string){
  var shasum = crypto.createHash('sha1');
  shasum.update(string);
  var d = shasum.digest('hex');
  return d;
}

exports.isObjectId = function (id) {
  return mongoose.Types.ObjectId.isValid(id);
}

exports.isObjectArray = function(obj){
  if(Object.prototype.toString.call(obj) === '[object Array]'){
    return true;
  }else{
    return false;
  }
}

exports.removeWhiteSpace = function(array){
  var length = array.length;
  for (var i = 0; i <length; i++) {
    accentsTidy(array[i], function(str) {
      array[i] = str;;
    });
  };
  return array;
}


exports.authorization = function (tokeEncode, id, role, next){
  var token = service.decodeToken(tokeEncode);
  if(token.rol == role && id === token.sub){
    next(null, token);
  }else if(token.rol == "admin"){
    next(null, token);
  }else{
    next({"error":"the rol is not client"});
  }
}

function accentsTidy(s, next){
    var r=s.toLowerCase();
    r = r.replace(new RegExp(/\s/g),"");
    r = r.replace(new RegExp(/[àáâãäå]/g),"a");
    r = r.replace(new RegExp(/[èéêë]/g),"e");
    r = r.replace(new RegExp(/[ìíîï]/g),"i");
    r = r.replace(new RegExp(/[òóôõö]/g),"o");
    r = r.replace(new RegExp(/[ùúûü]/g),"u");
    r = r.replace(new RegExp(/[ýÿ]/g),"y");
    r = r.replace(new RegExp(/\W/g),"");
    r = r.trim();
    r = r.toLowerCase();
    next(r);
}