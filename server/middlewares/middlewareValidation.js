"use strict";
var helpers = require('../controllers/helpers/helpers');
var isDefined = helpers.isDefined;

var structureLogin = {
  "email" : "",
  "password" : "",
  "rol": ""
};


function validateLogin(structure, next){
  console.log('validando', structure);
  var data = [];
  var testAuthorize = true;
  if(isDefined(structure.email)){
    data.push({
      "email":{
        "status" : "ok",
        "value" : structure.email
      }
    });
  }else{
    data.push({
      "email":{
        "status" : "error",
        "value" : "the email is undefined"
      }
    });
    testAuthorize = false;
  }

  if(isDefined(structure.password)){
    data.push({
      "password":{
        "status" : "ok",
        "value" : structure.password
      }
    });
  }else{
    data.push({
      "password":{
        "status" : "error",
        "value" : "the password is undefined"
      }
    });

    testAuthorize = false;
  }

  if(isDefined(structure.rol)){
    data.push({
      "rol":{
        "status" : "ok",
        "value" : structure.rol
      }
    });
  }else{
    data.push({
      "rol":{
        "status" : "error",
        "value" : "the rol is undefined"
      }
    });

    testAuthorize = false;
  }
  next(testAuthorize, data);
}

function resToIncorrectStructure(req, res, data){
  if(isDefined(req.query.errors) && req.query.errors=="verbose"){
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureLogin to be sent",
          "error" : {
            data:data
          },
          "help" : {
            "message" : " you structureLogin object must have the following structure",
            "structure": structureLogin
          }
        }
      });
  }else{
    res
      .status(400)
      .send({
        "error":{
          "reason" : "badstructure",
          "message" : "you have an error in structureLogin to be sent",
          "help" : {
            "message" : " you structureLogin object must have the following structure",
            "structure": structureLogin
          }
        }
      });
  }
}

module.exports = {
  validateLogin : validateLogin,
  resToIncorrectStructure : resToIncorrectStructure
}