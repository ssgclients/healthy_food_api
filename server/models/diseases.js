var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var diseaseSchema = new Schema({
  name : {type:String, required:true, unique:true},
  description : {type:String, default:''},
  recommended : [String],
  noRecommended : [String]
}, { versionKey: false });

exports.disease = mongoose.model('disease', diseaseSchema);