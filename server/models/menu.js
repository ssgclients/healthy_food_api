var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var plateSchema = new Schema({
  name : {type : String, default : ''},
  price : {type : Number, default: 0},
  image:{
    url:{type: String, default:'http://goo.gl/5qFCOR'},
    pubId:{type:String, default: 'v1454019945'}
  },
  ingredients:[String],
  restaurant: {type:Schema.Types.ObjectId, ref: 'restaurant'},
  rating : {
    sum : {type: Number, default: 0},
    countClients : {type: Number, default: 0}
  }
}, { versionKey: false });

var plateRatingSchema = new Schema({
  plate : {type:Schema.Types.ObjectId, ref: 'plate'},
  client: {type:Schema.Types.ObjectId, ref: 'client'},
  value : {type: Number, default: 0}
}, { versionKey: false });

exports.plate = mongoose.model('plate', plateSchema);
exports.plateRating = mongoose.model('plateRating', plateRatingSchema);
