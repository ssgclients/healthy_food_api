var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ingredientSchema = new Schema({
  name: {type:String, required:true, unique:true}
}, { versionKey: false });

exports.ingredient = mongoose.model('ingredient', ingredientSchema);