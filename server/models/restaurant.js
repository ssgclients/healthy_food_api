var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var restaurantSchema = new Schema({
  email : {type:String, required : true, unique:true},
  password : {type: String, required: true},
  name : {type : String, default : ''},
  phone : {type : String, default: ''},
  address: {type : String, default: ''},
  social:{
    twitter: {type : String, default: ''},
    facebook: {type : String, default: ''},
    instagram: {type : String, default: ''}
   },
  website: {type : String, default: ''},
  localization:{
     type: {type: String, default: 'Point', enum: ['Point']},
     coordinates: {type:[Number, Number],default: [0,0]}
  }
}, { versionKey: false });

restaurantSchema.index({localization:'2dsphere'});
exports.restaurant = mongoose.model('restaurant', restaurantSchema);