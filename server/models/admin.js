var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var adminSchema = new Schema({
  email : {type : String, required : true, unique:true},
  password : {type: String, required: true},
  name : {
    first : {type : String, default : ''},
    last : {type : String, default : ''}
  }
}, { versionKey: false });

exports.admin = mongoose.model('admin', adminSchema);