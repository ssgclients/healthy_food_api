"use strict";
var index = require("../controllers/api/");
var adminController = require("../controllers/api/admins/adminsController");
var clientController = require("../controllers/api/clients/clientsController");
var restaurantController = require("../controllers/api/restaurants/restaurantsController");
var ingredientController = require("../controllers/api/ingredients/ingredientController");
var diseaseController = require("../controllers/api/disease/diseasesController");
var plateMenuController = require("../controllers/api/plate/plateController");
var plateRatingController = require("../controllers/api/rating/plateRatingController");
var recommendController = require("../controllers/api/recommend/recommendController");
var middleware = require("../middlewares/middleware");
var multer = require('multer');
var upload = multer({dest:'uploads/'});

module.exports = function apiRoutes(app) {

  app.route("/api")
    .get(index.home);

  //path about admin
  app.route("/api/admins")
    .get(middleware.checkToken, adminController.search)
    .post(adminController.add);
  app.route("/api/admins/:admin")
    .get(middleware.checkToken, adminController.find)
    .put(middleware.checkToken, adminController.update)
    .delete(middleware.checkToken, adminController.remove);

  //path about clientes
  app.route("/api/clients")
    .get(middleware.checkToken, clientController.search)
    .post(clientController.add);
  app.route("/api/clients/:client")
    .get(middleware.checkToken, clientController.find)
    .put(middleware.checkToken, clientController.update)
    .delete(middleware.checkToken, clientController.remove);

  //path about restaurant
  app.route("/api/restaurants")
    .get(middleware.checkToken, restaurantController.search)
    .post(restaurantController.add);
  app.route("/api/restaurants/:restaurant")
    .get(middleware.checkToken, restaurantController.find)
    .put(middleware.checkToken, restaurantController.update)
    .delete(middleware.checkToken, restaurantController.remove);

  //path about ingredient
  app.route("/api/ingredients")
    .get(middleware.checkToken,ingredientController.search)
    .post(middleware.checkToken,ingredientController.add);
  app.route("/api/ingredients/:id")
    .get(middleware.checkToken, ingredientController.find)
    .put(middleware.checkToken, ingredientController.update)
    .delete(middleware.checkToken, ingredientController.remove);

  //path about diseases
  app.route("/api/diseases")
    .post(middleware.checkToken, diseaseController.add)
    .get(middleware.checkToken, diseaseController.search);
  app.route("/api/diseases/:disease")
    .get(middleware.checkToken,  diseaseController.find)
    .put(middleware.checkToken, diseaseController.update)
    .delete(middleware.checkToken, diseaseController.remove);

  //path about plate and menu by id equal email restaurant
  app.route("/api/:restaurant/plates")
    .get(middleware.checkToken, plateMenuController.searchByRestaurant)
    .post(middleware.checkToken, upload.single('file'), plateMenuController.add);

  //seach ingredient by id plate
  app.route("/api/plates")
    .get(plateMenuController.search);

  app.route("/api/plates/:plate")
    .get(plateMenuController.find)
    .delete(middleware.checkToken, plateMenuController.remove)
    .put(middleware.checkToken,upload.single('file'), plateMenuController.update);

  //paths about rating for plates
  app.route("/api/:plate/ratings")
    .post(middleware.checkToken,plateRatingController.add);

  app.get("/api/ratings",middleware.checkToken, plateRatingController.search);

  app.get("/api/recommendation", recommendController.search);
  //app.get("/api/prueba", recommendController.apiGoogle);

  //authenticated login
  app.route('/api/auth')
    .post(middleware.auth)
    .get(function (req, res){
      var service = require('../controllers/helpers/service');
      res.send({token:service.decodeToken(req.headers.authorization)});
    });

  //config: when don't exist the path
  app.use(function(req, res, next){
    res
      .status(404)
      .send({
        "error":{
          "error" : "RequestNotFound",
          "message" : "the path don´t exists, try another"

        }
      })
  });
}
