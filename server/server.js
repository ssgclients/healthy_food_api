"use strict";

var express = require("express");
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var logger = require('morgan');
var cors = require('cors');

var config = require('./config');
var apiRoutes = require("./routes/apiRoutes");



function serverListeningHandler(){
  console.log('Server listening at http://'+config.host +':'+config.port);
}

function start(){
    mongoose.connect('mongodb://'+config.db.user+':'+config.db.password+'@ds047315.mongolab.com:'+config.db.port+'/'+config.db.name);
    var db = mongoose.connection;
    db.on('error', console.error.bind(console, 'connection error: '));
    db.once('open', function callback(){
      app.use(bodyParser.urlencoded({ extended : true }));
      app.use(bodyParser.json());
      console.log('conexion establecida');
      console.log('Database HealthyFood');
      app.use(logger('dev'));
      app.use(cors());
      apiRoutes(app);
      app.listen(config.port, serverListeningHandler);
    });
}

var server = {
  start: start
}

module.exports = server;
